#!/usr/bin/env python3.6
import argparse
import binascii
import base64
import sys

parser = argparse.ArgumentParser()
parser.add_argument('text', nargs='+')
args = parser.parse_args(sys.argv[1:])

text = ' '.join(args.text)
text_binary = text.encode('ascii')
text_hexlified = binascii.hexlify(text_binary)
text_ascii = str(text_hexlified, 'ascii')
text_padded = '00'.join(text_ascii[i:i+2] for i in range(0, len(text_ascii), 2)) + '00'

user_id = '00000000 0000 0000 0000 000000000000'
user_id = 'C01ECB7C 0778 46A5 9968 E64583005854'
user_id = 'CBED18C1 A0D4 E711 81B2 CD140C1287E2'

#user_id = '6ABEBD0E 3532 E611 80D3 AC162DC085C0' #guild #4 

hex_user_str = f'08 {user_id} {text_padded} 0000'.replace(' ', '')


text_b64 = base64.b64encode(binascii.unhexlify(hex_user_str))
text_b64_str = str(text_b64, 'ascii')
text_chatcode = f'[&{text_b64_str}]'
print(text_chatcode)

#!/usr/bin/env python3

import requests
import time
import subprocess
import argparse
import sys

def notify(url, exit_after=True):
    print(url + '               ' + '#'*500)
    subprocess.call(['paplay', '/usr/share/sounds/freedesktop/stereo/suspend-error.oga'])
    if exit_after:
        exit(0)

parser = argparse.ArgumentParser()
parser.add_argument('-b', '--boundary',type=int, help='Limit')
parser.add_argument('-w', '--wait-new', action='store_true', help='Disregard limit, notify about first new post after start.')
args = parser.parse_args(sys.argv[1:])

print(args)
old_size = None
while True:
    gw2json=requests.get('https://en-forum.guildwars2.com/categories/game-release-notes.json').json()
    found_size = len(gw2json['Discussions'])
    print('found {} @ https://en-forum.guildwars2.com/categories/game-release-notes'.format(found_size))
    if not args.wait_new and len(gw2json['Discussions']) > args.boundary:             
        notify(gw2json['Discussions'][0]['Url'])
    elif args.wait_new and old_size is not None and found_size > old_size:
        notify(gw2json['Discussions'][0]['Url'])
    old_size = found_size
    time.sleep(5)



#!/usr/bin/env python3

import fire
import logging
import subprocess

def encode(infile, outfile):
    logging.info('infile:%s outfile:%s', infile, outfile)
    subprocess.run(['ffmpeg', '-i', infile, '-c:v', 'libvpx-vp9', '-b:v', '0', '-crf', '30', '-pass', '1', '-an', '-f', 'webm', '/dev/null'])
    subprocess.run(['ffmpeg', '-i', infile, '-c:v', 'libvpx-vp9', '-b:v', '0', '-crf', '30', '-pass', '2', '-c:a', 'libopus', outfile])

def trim(infile, outfile, starttime, endtime):
    logging.info('infile:%s outfile:%s starttime: %s endtime: %s', infile, outfile, starttime, endtime)
    subprocess.run(['ffmpeg', '-noaccurate_seek', '-ss', starttime, '-i', infile, '-to', endtime, '-c', 'copy', '-copyts', outfile])

if __name__ == '__main__':
    fire.Fire({
        'encode': encode,
        'trim': trim
    })


#!/bin/bash

pushd "$1" || exit

rm d3d9.dll
rm d3d9_arcdps_buildtemplates.dll
rm d3d9_arcdps_extras.dll 


if [[ "$2" == "dev" ]]; then
	wget https://www.deltaconnected.com/arcdps/dev/d3d9.dll
	wget https://www.deltaconnected.com/arcdps/dev/d3d9_arcdps_buildtemplates.dll
	wget https://www.deltaconnected.com/arcdps/dev/d3d9_arcdps_extras.dll
else
	wget https://www.deltaconnected.com/arcdps/x64/d3d9.dll
	wget https://www.deltaconnected.com/arcdps/x64/buildtemplates/d3d9_arcdps_buildtemplates.dll
	wget https://www.deltaconnected.com/arcdps/x64/extras/d3d9_arcdps_extras.dll
fi

popd || exit

TARGET_PARTY=0 # 0 indexed
CAN_KICK=0 # 1=yes, 0=no
IS_SQUAD=1 # 1=yes, 0=no
TARGET_SQUAD=1
TARGET_SUBSQUAD_DIFF=1

# Defs

PARTY_START=120
PARTY_STEP=30

OPTION_STEP=23

SQUAD_START=145
SQUAD_STEP=25
SQUAD_SUBSQUAD_ADD=5

if [[ $IS_SQUAD -eq 1 ]]; then
	POS_OFFSET=$(( TARGET_SQUAD * SQUAD_STEP + TARGET_SUBSQUAD_DIFF * SQUAD_SUBSQUAD_ADD ))
	START_POS=$SQUAD_START
else
	POS_OFFSET=$(( TARGET_PARTY * PARTY_STEP  ))
	START_POS=$PARTY_START
fi

for i in {1..100}; do
	read -n 1 -s -r -p "$i"
	echo
	for k in {1..200}; do
		xdotool mousemove 60 $((START_POS + POS_OFFSET)) click 3
		sleep 0.2
		if [[ $CAN_KICK -eq 1 ]]; then
			JOIN_POS=$((START_POS + OPTION_STEP/3 + OPTION_STEP*4 + POS_OFFSET))
		else
			JOIN_POS=$((START_POS + OPTION_STEP/3 + OPTION_STEP*3 + POS_OFFSET))
		fi
		xdotool mousemove 149 $JOIN_POS click 1
		sleep 0.2
		xdotool mousemove 940 580 click 1
		xdotool mousemove 1752 670 click --repeat 2 1
		sleep 2
	done
done

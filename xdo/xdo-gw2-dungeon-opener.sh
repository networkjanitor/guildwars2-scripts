# For spam-clicking the place of the "join explorable mode" button when you walk up to a dungeon portal. 
# Useful to enter closed-off dungeons like CoE where the portal is still enabled just access is closed off,
# but you can jump over the barrier with springer and then fall down to see the window for a split second.

for i in {1..250}; do
        echo $i
	sleep 0.01	
        xdotool mousemove 817 566 click --repeat 2 1 
done

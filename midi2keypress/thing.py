#!/usr/bin/env python3


import sys
import time
import argparse
import subprocess
import time

import rtmidi
from rtmidi.midiutil import open_midiinput
from rtmidi.midiconstants import (CHANNEL_PRESSURE, CONTROLLER_CHANGE, NOTE_ON, NOTE_OFF,
    PITCH_BEND, POLY_PRESSURE, PROGRAM_CHANGE)


class MidiInputHandler:
    def __init__(self, port, watch_note_length, flute, pipe_organ):
        self.port = port
        self.current_octave = 0
        self.next_note_is_higher_c = False
        self.is_flute = flute
        self.is_pipe_organ = pipe_organ
        self.watch_note_length = watch_note_length

        self.key_config = {
                'C':                '1',
                'D':                '2',
                'E':                '3',
                'F':                '4',
                'G':                '5',
                'A':                'q',
                'H':                'e',
                '+C':               'r',
                'OctaveFlute':      't',
                'OctaveDown':       't',
                'OctaveUp':         'x',
                'OctaveDownOrgan':  'r',
                'OctaveUpOrgan':    't'
        }

    def __call__(self, event, data=None):
        event, deltatime = event

        if event[0] < 0xF0:
            # all the stuff before "System Exclusive" range
            channel = (event[0] & 0xF) + 1
            status = event[0] & 0xF0
        else:
            # System Exclusive range
            status = event[0]
            channel = None

        data1 = data2 = None
        num_bytes = len(event)

        if num_bytes >= 2:
            data1 = event[1]
        if num_bytes >= 3:
            data2 = event[2]

        #print("[%s] CH:%2s %02X %s %s %r" % (self.port, channel or '-', status, data1, data2 or '', event))
        self.process_midi_key(event)
    
    def process_midi_key(self, event):
        # if 1st byte is between 0x90 and 0x9F (NoteOn range)
        note_on = (event[0] & 0xF0) == 0x90
        note_off = (event[0] & 0xF0 ) == 0x80
        # the note id
        note_id = event[1] 

        if self.is_flute and note_on:
            octave = self.note_to_octave(note_id)
            note = self.note_id_to_note(note_id)
            self.emit_new_flute_octave(octave, note)
            if self.next_note_is_higher_c:
                self.next_note_is_higher_c = False
                self.emit_key_press(self.key_config['+C'])
            else:
                self.emit_key_press(self.key_config[note])
            return
        elif self.is_flute:
            return

        if self.is_pipe_organ and note_on:
            print('pipe')
            octave = self.note_to_octave(note_id)
            note = self.note_id_to_note(note_id)
            self.emit_new_pipe_organ_octave(octave, note)
            self.emit_key_press(self.key_config[note])

        elif self.is_pipe_organ:
            return
        print("wrong")
        if note_on and not self.watch_note_length:
            octave = self.note_to_octave(note_id)
            note = self.note_id_to_note(note_id)
            self.emit_new_octave(octave, note)
            if self.next_note_is_higher_c:
                self.next_note_is_higher_c = False
                self.emit_key_press(self.key_config['+C'])
            else:
                self.emit_key_press(self.key_config[note])

        elif note_on and self.watch_note_length:
            octave = self.note_to_octave(note_id)
            note = self.note_id_to_note(note_id)
            self.emit_new_octave(octave, note)
            self.emit_key_down(self.key_config[note])

        elif note_off and self.watch_note_length:
            note = self.note_id_to_note(note_id)
            self.emit_key_up(self.key_config[note])

   

    def note_id_to_note(self, note_id) -> str:
        # remove octave part
        note_from_zero = note_id % 12

        # chromatic to diatonic, but also into str format
        key_switch = { 
                0: 'C', 
                1: 'C', 
                2: 'D',
                3: 'D',
                4: 'E',
                5: 'F',
                6: 'F',
                7: 'G',
                8: 'G',
                9: 'A',
                10: 'A',
                11: 'H'
        }
        return key_switch[note_from_zero]

    def note_to_octave(self, note_id: int) -> int:
        semitones_in_octave = 12
        return int(note_id / semitones_in_octave)

    def emit_key_press(self, key):
        print(f"pressing {key} now")
        subprocess.call(['xdotool', 'key', key])

    def emit_key_down(self, key):
        print(f"keydown {key}")
        subprocess.call(['xdotool', 'keydown', key])

    def emit_key_up(self, key):
        print(f"keyup {key}")
        subprocess.call(['xdotool', 'keyup', key])


    def emit_new_octave(self, new_octave, next_note):
        octave_diff = new_octave - self.current_octave
        octave_key = self.key_config['OctaveUp'] if octave_diff > 0 else self.key_config['OctaveDown']
       
        print(f'diff: {octave_diff}')
        print(f'new: {new_octave}')

        if not self.watch_note_length:
            if octave_diff == 1 and next_note == 'C':
                self.next_note_is_higher_c = True
                return 
            elif octave_diff >= 2 and next_note == 'C':
                self.next_note_is_higher_c = True
                new_octave -= 1

        self.current_octave = new_octave
        for i in range(abs(octave_diff)):
            self.emit_key_press(octave_key)
            time.sleep(0.1)

    def emit_new_flute_octave(self, new_octave, next_note):
        octave_diff = new_octave - self.current_octave
        if octave_diff > 0 and next_note == 'C':
            self.next_note_is_higher_c = True
            return
        elif octave_diff != 0:
            self.emit_key_press(self.key_config['OctaveFlute'])
        self.current_octave = new_octave

    def emit_new_pipe_organ_octave(self, new_octave, next_note):
        octave_diff = new_octave - self.current_octave
        octave_key = self.key_config['OctaveUpOrgan'] if octave_diff > 0 else self.key_config['OctaveDownOrgan']

        self.current_octave = new_octave
        for i in range(abs(octave_diff)):
            self.emit_key_press(octave_key)
            time.sleep(0.1)



def parse(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--list-ports', action='store_true', help='List all available ports to choose as input from.')
    parser.add_argument('-p', '--port', help='MIDI input port name or number')
    parser.add_argument('-w', '--watch-note-length', action='store_true', help='Disable 8=1 optimization for chords. Use this when you want to play flute, horn or tuba.')
    parser.add_argument('-f', '--flute', action='store_true', help='Use this for flute playing.')
    parser.add_argument('-o', '--pipe-organ', action='store_true', help='Use this for pipe organ playing.')
    
    return parser.parse_args(args)

def main():

    args = parse(sys.argv[1:])
    print(args)

    if args.list_ports:
        inmidi = rtmidi.MidiIn()
        for index,port in enumerate(inmidi.get_ports()):
            print(f'{index} | {port}')
        exit(0)
    

    try:
        midiin, port_name = open_midiinput(
            args.port,
            use_virtual=True,
        #    api=BACKEND_MAP.get(args.backend, rtmidi.API_UNSPECIFIED),
            client_name='midi2command',
            port_name='MIDI input')
    except (IOError, ValueError) as exc:
        return "Could not open MIDI input: %s" % exc
    except (EOFError, KeyboardInterrupt):
        return


    print("Attaching MIDI input callback handler.")
    midiin.set_callback(MidiInputHandler(port_name, args.watch_note_length, args.flute, args.pipe_organ))


    print("Entering main loop. Press Control-C to exit.")
    try:
        # just wait for keyboard interrupt in main thread
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print('')
    finally:
        midiin.close_port()
        del midiin

if __name__ == '__main__':
    main()

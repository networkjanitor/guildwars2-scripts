#!/usr/bin/env python3

import argparse
import sys

def parse(args):
    p = argparse.ArgumentParser()
    p.add_argument('file')
    p.add_argument('--schema')
    return p.parse_args(args)

class NoteConverter:
    
    def convert_file(self, input_path, output_path, schema):
        if schema == 'gw2mb':
            self.convert_file_gw2mb(input_path, output_path)
        else:
            self.convert_file_fallback(input_path, output_path)
        
    def convert_file_gw2mb(self, input_path, output_path):
        replacements = {
                '1': 'C',
                '2': 'D',
                '3': 'E',
                '4': 'F',
                '5': 'G',
                '6': 'A',
                '7': 'H',
                '8': 'C',
        }

        current_octave = 2

        output = []
        with open(input_path, 'r') as in_f:
            for line in in_f:
                if line.startswith('Created using http://gw2mb.com'):
                    pass
                elif not line.startswith('#'):
                    for character in line:
                        if character == '▲':
                            # up it goes
                            current_octave += 1
                        elif character == '▼':
                            # down it goes
                            current_octave -= 1
                        else:
                            # must be a key or something irrelevant
                            try:
                                new_char = replacements[character]

                                if character == '8':
                                    # 8 = 0+1, eh ? :^)
                                    output.append(f'{new_char}{current_octave+1}')
                                else:
                                    # business as usual
                                    output.append(f'{new_char}{current_octave}')
                            
                            except KeyError:
                                # oops, not a key - gotta preserve it anyway
                                new_char = character
                                output.append(new_char)

                else:
                    output.append(line)
        with open(output_path, 'w') as out_f:
            out_f.write(''.join(output))
        print(output)


    def convert_file_fallback(self, input_path, output_path):
        
        replacements = {
                '1': 'C',
                '2': 'D',
                '3': 'E',
                '4': 'F',
                '5': 'G',
                '6': 'A',
                '7': 'H',
                '8': '+C',
                '9': '-',
                '0': '+'
        }

        output = []
        with open(input_path, 'r') as in_f:
            for line in in_f:
                if not line.startswith('#'):
                    temp_output = line
                    for k,v in replacements.items():
                        temp_output = temp_output.replace(k, v)
                    output.append(temp_output)
                else:
                    output.append(line)
        with open(output_path, 'w') as out_f:
            out_f.write(''.join(output))
        print(output)


def main():
    args = parse(sys.argv[1:])
    print(args)

    nc = NoteConverter()
    nc.convert_file(args.file, args.file + '.actual_notes', args.schema)


if __name__ == '__main__':
    main()
    

